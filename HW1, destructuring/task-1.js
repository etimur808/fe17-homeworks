"use strict"

const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

const allUsers= [...clients1, ...clients2];
const currentClients = [...new Set(allUsers)];
console.log(currentClients);
