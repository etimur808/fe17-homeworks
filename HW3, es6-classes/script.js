class Employee {

    constructor(options) {
        this.name = options.name
        this.age = options.age
        this.salary = options.salary


    }

    get newName(){
        return this.name;
    }
    set newName(newName){
        this.name = newName;
    }
    get newAge(){
        return this.age
    }
    set newAge(newAge){
        this.age = newAge
    }
    get newSalary(){
        return this.salary
    }
    set newSalary(newSalary){
        this.salary = newSalary
    }



}

class Programmer extends Employee{

    constructor(options) {
        super(options);

        this.lang = options.lang
    }

    get newSalary(){
        this.salary = this.salary * 3;
        return this.salary
        // return this.salary * 3
    }

}


 const programmer = new Programmer({
     name: "timur",
     age: 20,
     salary: 200000,
     lang: {
        eng: "English",
         ru: "Russian",
         ua: "Ukrainian"
     }
 });

const programmer2 = new Programmer({
    name: "halib",
    age: 17,
    salary: 5,
    lang: {
        eng: "English",
    }
});



