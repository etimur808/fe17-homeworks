import React, {Component} from 'react';
import Modal from "./components/Modal";
import Button from "./components/Button";
import ProductsList from "./components/ProductsList"


class App extends Component {

    state = {
        isOpen: false,
        items: []
    };


    // getFavourites(){
    //
    //     window.addEventListener('load', ()=>{
    //         let collection = document.getElementsByClassName('products')[0].childNodes;
    //         let prod = Array.from(collection)
    //         let activeStar = []
    //
    //         prod.forEach((el)=>{
    //              let item = el.childNodes[1].innerText
    //
    //             for (let i = 0; i< localStorage.length; i++){
    //                 if (item === localStorage.key(i)){
    //                     activeStar.push(el.lastChild)
    //                 }
    //             }
    //
    //         })
    //
    //         activeStar.forEach((i)=>{
    //             i.classList.add('active')
    //             console.log(i);
    //         })
    //
    //     })
    // }


    componentDidMount() {
            fetch('products.json')
                .then(res => res.json())
                .then(data => this.setState({items: data}))

        // this.getFavourites()
    }



    openModal = () => {
        this.setState({
            isOpen: true
        })
    }

    closeModal = () => {
        this.setState({
            isOpen: false
        })
    }

    modalContent = (title, description, closeBtnClassName, headerClassName, contentClassName, okBtnClass, cancelBtnClass, itemData) => {
        this.title = title
        this.description = description
        this.btnClassName = closeBtnClassName
        this.headerClassName = headerClassName
        this.contentClassName = contentClassName
        this.okBtnClass = okBtnClass
        this.cancelBtnClass = cancelBtnClass
        this.itemData = itemData

    }


    render() {
        return (

            <div>
                {this.state.isOpen && <Modal
                    title={this.title}
                    description={this.description}
                    btnClass={this.btnClassName}
                    action={this.closeModal}
                    headerClass={this.headerClassName}
                    contentClass={this.contentClassName}
                    okBtnClass={this.okBtnClass}
                    cancelBtnClass={this.cancelBtnClass}
                    itemData={this.itemData}
                />}

                <div className={'button-wrapper'}>


                    <Button
                        modal={this.modalContent}
                        modalContent={{
                            title: 'Do you want to save this file',
                            description: 'This file can contain viruses. Are you sure you want to install this file?',
                            btnClassName: 'fas fa-times modal__firstCloseBtn',
                            headerClassName: 'modal__header modal__firstHeader',
                            contentClassName: 'modal__content modal__firstModalContent',
                            okBtnClassName: 'modal__bodyBtn modal__okFirstBtn',
                            cancelBtnClassName: 'modal__bodyBtn modal__cancelFirstBtn'
                        }}
                        action={this.openModal}
                        state={this.state}
                        btnText={"first modal"}
                        className={'first-modal'}/>

                    <Button
                        modal={this.modalContent}
                        modalContent={{
                            title: 'Do you want to delete this file?',
                            description: 'Once you delete this file, it won’t be possible to undo this action. Are you sure you want to delete it?',
                            btnClassName: 'fas fa-times modal__secondCloseBtn',
                            headerClassName: 'modal__header modal__secondHeader',
                            contentClassName: 'modal__content modal__secondModalContent',
                            okBtnClassName: 'modal__bodyBtn modal__okSecondBtn',
                            cancelBtnClassName: 'modal__bodyBtn modal__cancelSecondBtn'

                        }}
                        action={this.openModal}
                        state={this.state}
                        btnText={"Second modal"}
                        className={'second-modal'}/>

                </div>

                    <ProductsList action={this.openModal} modalContent={this.modalContent} content={this.state.items}/>

            </div>
        );

    }
}

export default App;