import React, {Component} from 'react';
import {render} from "react-dom";
import App from "../App";
import ProductItem from "./ProductItem";
import PropTypes from 'prop-types'

class ProductsList extends Component {

    render() {
        const products = this.props.content

        return (
            <div className={'products'}>
                {products.map(item => {
                    return <ProductItem className='product-item'
                                        title={item.modelName}
                                        price={item.price}
                                        passForImg={item.passForImage}
                                        vendorCode={item.vendorCode}
                                        color={item.color}
                                        action = {this.props.action}
                                        modal={this.props.modalContent}
                    />
                })}
            </div>
        );
    }
}

ProductsList.propTypes = {
    action: PropTypes.func,
    modalContent: PropTypes.func,
    content: PropTypes.array
}


export default ProductsList;