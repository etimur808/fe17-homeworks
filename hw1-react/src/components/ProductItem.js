import React, {Component} from 'react';
import Button from "./Button";
import PropTypes from 'prop-types'

class ProductItem extends Component {

    addToFavourites = (e) =>{

        let activeItem = e.target.closest(".product-item").childNodes;
        let [ , name, price, color, vendorCode] = activeItem;

        let favItem = {
            name: name.innerText,
            price: price.innerText,
            color: color.innerText,
            vendorCode: vendorCode.innerText
        }

        if (e.target.className.includes('active')){
            localStorage.removeItem(`Add to fav ${name.innerText}`)
        }
        else{
            localStorage.setItem(`Add to fav ${name.innerText}` , JSON.stringify(favItem))
        }

        e.target.classList.toggle("active")
    }


    render() {
        const{className, passForImg, title, price, vendorCode, color} = this.props
        const action = this.props.action
        const modalContent = this.props.modal



        return (

            <div className={className}>
                <img className={'product-item__image'} src={passForImg} alt="img"/>
                <h4 className={'product-item__name'}>{title}</h4>
                <p className={'product-item__price'}>{'Price: ' + price + ' грн'}</p>
                <p className={'product-item__color'}>{"Color: " + color}</p>
                <p className={'product-item__vendorCode'}>{"Vendor Code: " + vendorCode}</p>
                <p></p>
                <Button
                    btnText={'Add to cart'}
                    className={'product-item__addToCartButton'}
                    modal={modalContent}
                    action={action}
                    modalContent={{
                        title: 'Add to cart this item?',
                        description: '',
                        btnClassName: 'fas fa-times modal__addToCartCloseBtn',
                        headerClassName: 'modal__header modal__addToCardHeader',
                        contentClassName: 'modal__content modal__addToCartModalContent',
                        okBtnClassName: 'modal__bodyBtn modal__addToCartOkBtn',
                        cancelBtnClassName: 'modal__bodyBtn modal__addToCartCancelBtn',
                        itemData: {
                            title: title,
                            price: price,
                            vendorCode: vendorCode,
                            color: color
                        }
                    }}

                />

                <i onClick={this.addToFavourites} className="fas fa-star product-item__star"/>

            </div>
        );
    }
}

ProductItem.propTypes = {
    className: PropTypes.string,
    title: PropTypes.string,
    price: PropTypes.number,
    passForImg: PropTypes.string,
    vendorCode: PropTypes.number,
    color: PropTypes.string,
    action: PropTypes.func,
    modal: PropTypes.func
}

ProductItem.defaultProps = {
    className: 'product-item',
    title: 'Model name',
    price: 1111,
    passForImg: '',
    vendorCode: 1111111111,
    color: 'none'
}
export default ProductItem;