import React, {Component} from 'react';
import Modal from "./Modal";
import {render} from "react-dom";
import PropTypes from "prop-types";

class Button extends Component {
    constructor(props) {
        super(props);
        this.btnText = props.btnText;
        this.className = props.className;
        this.title = props.title
    }

    openModal=(e)=>{
        this.props.action()
        this.props.modal(this.props.modalContent.title,
            this.props.modalContent.description,
            this.props.modalContent.btnClassName,
            this.props.modalContent.headerClassName,
            this.props.modalContent.contentClassName,
            this.props.modalContent.okBtnClassName,
            this.props.modalContent.cancelBtnClassName,
            this.props.modalContent.itemData
    )};

    render() {
        return (
            <div>
                <button onClick={this.openModal} className={this.className}>{this.btnText}</button>
            </div>
        );
    }
}

Button.propTypes = {
    modal: PropTypes.func,
    modalContent: PropTypes.object,
    action: PropTypes.func,
    state: PropTypes.object,
    btnText: PropTypes.string,
    className:  PropTypes.string
}

Button.defaultProps = {
    modalContent: {
        title: 'modal is not found',
        description: '',
        btnClassName: 'fas fa-times',
        headerClassName: 'modal__header',
        contentClassName: 'modal__content',
        okBtnClassName: 'modal__bodyBtn modal__okFirstBtn',
        cancelBtnClassName: 'modal__bodyBtn modal__cancelFirstBtn'
    },
    btnText: "button",
    className: 'button'
}

export default Button;