import React from "react";
import Button from "./components/Button";
import {render} from "react-dom";
import buttonStyles from './components/Button.scss'
import modalStyles from './components/Modal.scss'
import Modal from "./components/Modal";
import itemsStyles from './components/ProductItem.scss'
import productList from "./components/ProductList.scss"
import App from "./App";
import {BrowserRouter as Router, Route, Link} from "react-router-dom";
import Cart from "./components/Cart/Cart";
import Favourites from "./components/Favourites/Favourites";
import HeaderMenu from './components/HeaderMenu.scss'

function Create() {

    return (
    <Router>
            <div>
                <nav className={'header-menu'}>
                    <Link className={'header-menu__link'} to='/cart'>Cart</Link>
                    <Link className={'header-menu__link'} to='/'>Shop</Link>
                    <Link className={'header-menu__link'} to='/favourites'>Favourites</Link>
                </nav>
            </div>

        <Route exact path='/' component={App}/>
        <Route exact path='/cart' component={Cart}/>
        <Route exact path='/favourites' component={Favourites}/>

    </Router>


    )

}

render(<Create/>, document.getElementById('root'))


