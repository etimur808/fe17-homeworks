import React, {useState, useEffect} from 'react'
import ProductList from "../../components/ProductList/ProductList";
import PropTypes from 'prop-types'
import Modal from "../../components/Modal/Modal";

function Cart(props) {


    let [cartItems, getCartItems] = useState(null)
    let storageCartItems = []

    useEffect(() => {
        let storageItems = {...localStorage}

        for (let key in storageItems) {
            if (key.includes('Cart')) {
                storageCartItems.push(JSON.parse(storageItems[key]))
                getCartItems(storageCartItems)
            }
        }
    }, [])

    return (
        <div>

            {cartItems !== null && <ProductList
                buttonContent={{
                    className: 'product-item__removeCartBtn fas fa-times',
                    modalTitle: 'Delete this cart?',
                    modalRemoveFromCart: 'modal_bodyBtn modal_removeCartBtn'
                }}
                data={cartItems}
                modal={props.modal}
                action={props.action}
            />}

            {props.modalState !== false && <Modal action={props.action} content={props.content}/>}

        </div>
    )
}


export default Cart