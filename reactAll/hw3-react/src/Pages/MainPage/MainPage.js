import React from 'react'
import ProductList from "../../components/ProductList/ProductList";
import Modal from "../../components/Modal/Modal";
import PropTypes from 'prop-types'


function MainPage(props) {


    return (
        <div>
            <ProductList
                buttonContent={{
                    btnText: 'Add to cart',
                    className: 'product-item__addToCartButton',
                    modalTitle: 'Add to cart this item?',
                    modalRemoveFromCart: 'modal_bodyBtn modal_addToCartItem'
                }}
                modal={props.modal}
                action={props.action}
                data={props.data}/>

            {props.modalState !== false && <Modal
                action={props.action}
                content={props.content}/>}
        </div>
    )
}

MainPage.propTypes = {
    modal: PropTypes.func,
    action: PropTypes.func,
    data: PropTypes.array,
    modalState: PropTypes.bool,
    content: PropTypes.object
}


export default MainPage