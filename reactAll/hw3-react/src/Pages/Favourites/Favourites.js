import React, {useEffect, useState} from 'react'
import ProductList from "../../components/ProductList/ProductList";
import Modal from "../../components/Modal/Modal";



function Favourites(props){


    let [favItems, getFavItems] = useState(null)
    let favouritesItems = []

    useEffect(()=>{
        let itemsFromStorage = {...localStorage}
        for (let key in itemsFromStorage){
            if(key.includes('Favourites')){
                let item = JSON.parse(localStorage.getItem(key))
                favouritesItems.push(item)
                getFavItems(favouritesItems)
            }
        }
    }, [])

    return(


        <div>
            {favItems !== null && <ProductList
                buttonContent={{
                    btnText: 'Add to cart',
                    className: 'product-item__addToCartButton',
                    modalTitle: 'Add to cart this item?',
                    modalRemoveFromCart: 'modal_bodyBtn modal_addToCartItem'
                }}
                data={favItems}
                modal={props.modal}
                action={props.action}
            />}

            {props.modalState !== false && <Modal action={props.action} content={props.content}/>}

        </div>
    )
}

export default Favourites