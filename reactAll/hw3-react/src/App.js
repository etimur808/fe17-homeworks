import React, {useState, useEffect, useRef} from 'react'
import ProductList from "./components/ProductList/ProductList";
import Modal from "./components/Modal/Modal";
import Routing from './routing/Routing'

function App() {


    let [productsData, getProductsData] = useState(null)
    let [modalContent, getModalContent] = useState(null)
    let [btnAction, getBtnAction] = useState(false)

    useEffect(()=>{
      fetch('products.json')
          .then(res => res.json())
          .then(data=> getProductsData(data))

    }, [])


      return (


      <div className="App">
          {productsData !== null && <Routing

              action={getBtnAction}
              content={modalContent}
              data={productsData}
              modal={getModalContent}
              modalState={btnAction}/>}

      </div>
    );
}

export default App;
