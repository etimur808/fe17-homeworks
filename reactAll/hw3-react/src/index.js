import React from 'react';
import ReactDOM from 'react-dom';
import MainPage from "./Pages/MainPage/MainPage";
import Cart from "./Pages/Cart/Cart";
import Favourites from "./Pages/Favourites/Favourites";
import App from "./App";
import itemStyle from '../src/components/ProductItem/ItemStyle.scss'
import listStyle from '../src/components/ProductList/ListStyle.scss'
import modalStyle from '../src/components/Modal/ModalStyle.scss'
import ButtonStyle from '../src/components/Button/ButtonStyle.scss'



ReactDOM.render(
  <React.StrictMode>
      <App />
  </React.StrictMode>,
  document.getElementById('root')
);

