import React from 'react'
import {BrowserRouter as Router, Switch, Route,Link} from "react-router-dom";
import Cart from "../Pages/Cart/Cart";
import MainPage from "../Pages/MainPage/MainPage";
import Favourites from "../Pages/Favourites/Favourites";
import PropTypes from 'prop-types'

function Routing(props){


    return(

        <Router>
          <div className="header-menu">
              <Link className="header-menu__link" to='/cart'>Cart</Link>
              <Link className="header-menu__link" to='/'>Shop</Link>
              <Link className="header-menu__link" to='/favourites'>Favourites</Link>
          </div>


            <Switch>

                <Route exact path={'/cart'}>

                    <Cart
                        modal={props.modal}
                        content={props.content}
                        modalState={props.modalState}
                        action={props.action}

                    />

                </Route>

                <Route exact path={'/'}>

                    <MainPage
                        modal={props.modal}
                        content={props.content}
                        modalState={props.modalState}
                        action={props.action}
                        data={props.data}/>

                </Route>

                <Route exact path={'/favourites'}>

                    <Favourites
                        content={props.content}
                        modalState={props.modalState}
                        modal={props.modal}
                        action={props.action}
                    />

                </Route>

            </Switch>
    </Router>

    )
}


Routing.propTypes = {
    action: PropTypes.func,
    content: PropTypes.object,
    data: PropTypes.array,
    modal: PropTypes.func,
    modalState: PropTypes.bool
}


export default Routing