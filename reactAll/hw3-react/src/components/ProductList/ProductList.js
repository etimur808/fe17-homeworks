import React, {useEffect} from 'react'
import ProductItem from "../ProductItem/ProductItem";
import PropTypes from 'prop-types'

function ProductList(props){

    let data = props.data




    return(

        <div className={'products'}>
            {data.map(item=>{
                return <ProductItem
                        buttonContent={props.buttonContent}
                        action = {props.action}
                        modalContent = {props.modal}
                        productItem={{
                            modelName: item.modelName,
                            price: item.price,
                            passForImage: item.passForImage,
                            vendorCode: item.vendorCode,
                            color: item.color,
                            // action: props.action,
                            // modalContent: props.modal
                        }}
                />
            })}

        </div>
     )

}

ProductList.propTypes = {
    action: PropTypes.func,
    modal: PropTypes.func,
    data: PropTypes.array
}


export default ProductList
