import React from 'react'
import PropTypes from 'prop-types'

function Button(props){

    function openModal(){
        props.action(true);
        props.modal(props.modalContent)
    }

    return(
        <div>
        <button onClick={openModal} className={props.className}>
            {props.btnText}
        </button>
        </div>
    )
}

Button.propTypes = {
    action: PropTypes.func,
    modal: PropTypes.func,
    className: PropTypes.string,
    btnText: PropTypes.string
}

export default Button

