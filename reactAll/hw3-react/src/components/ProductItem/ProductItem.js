import React, {useEffect, useState} from 'react'
import Button from '../Button/Button'
import Proptypes from 'prop-types'


function ProductItem(props){


    let {modelName, color, price, passForImage, vendorCode} = props.productItem

    let [isFavourite, setFavourite] = useState(false)

    useEffect(()=>{
        let storageItems = {...localStorage}
        let itemsKey = []
        for (let key in storageItems){
            itemsKey.push(key)
        }
        let favItem = itemsKey.some(storageItem => storageItem === `Favourites ${modelName}`)
        setFavourite(favItem)

    }, [])

    function favourites(e){

        let star = e.target.classList
        if (star.contains('active')){
            star.remove('active')
            localStorage.removeItem(`Favourites ${modelName}`)
        }
        else{
            localStorage.setItem(`Favourites ${modelName}`, JSON.stringify(props.productItem))
            e.target.classList.add('active')
        }

    }



    return(
        <div className={'product-item'}>
            <img className={'product-item__image'} src={passForImage} alt=""/>
            <h4 className={'product-item__name'}>{modelName}</h4>
            <p className={'product-item__price'}>{`Price: ${price}`}</p>
            <p className={'product-item__color'}>{`Color: ${color}`}</p>
            <p className={'product-item__vendorCode'}>{`Vendor Code ${vendorCode}`}</p>

            <Button btnText={props.buttonContent.btnText}
                    className={props.buttonContent.className}
                    action={props.action}
                    modal={props.modalContent}
                    modalContent={{
                        title: props.buttonContent.modalTitle,
                        description: '',
                        btnClassName: 'fas fa-times modal__addToCartCloseBtn',
                        headerClassName: 'modal__header modal__addToCartHeader',
                        contentClassName: 'modal__content modal__addToCartModalContent',
                        okBtnClassName: props.buttonContent.modalRemoveFromCart,
                        cancelBtnClassName: 'modal__bodyBtn modal__addToCartCancelBtn',
                        item: props.productItem
                    }}
            />

            <i onClick={favourites} className={`fas fa-star product-item__star ${isFavourite ? 'active' : '' }`}/>

        </div>
    )

}

ProductItem.propTypes ={
    productItem: Proptypes.object,
    action: Proptypes.func,
    modalContent: Proptypes.func
}

ProductItem.defaultProps = {
    action : function noModal(){},
    modalContent: function noContent(){}
}

export default ProductItem