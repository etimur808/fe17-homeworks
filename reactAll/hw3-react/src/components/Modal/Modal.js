import React from 'react'
import PropTypes from 'prop-types'

function Modal(props) {

    function closeModal(e){
        let close = e.target.className
        if (close === 'modal' || close === props.content.cancelBtnClassName || close === props.content.btnClassName){
            props.action(false)
        }
    }


    function cart(){
        if (props.content.okBtnClassName.includes('modal_addToCartItem')){
            localStorage.setItem(`Cart ${props.content.item.modelName}`, JSON.stringify(props.content.item))
            props.action(false)
        }
        else{
            localStorage.removeItem(`Cart ${props.content.item.modelName}`)
            props.action(false)
        }


    }

    return(


        <div>

             <div onClick={closeModal} className={'modal'}>
                 <div className={props.content.contentClassName}>
                     <div className={props.content.headerClassName}>
                         <h1 className={'modal__title'}>{props.content.title}</h1>
                         <button onClick={closeModal} className={props.content.btnClassName}/>
                     </div>
                     <div className="modal__body">
                         <p>{props.content.description}</p>

                         <div className="modal__bodyButtons">
                             <button onClick={cart} className={props.content.okBtnClassName}>OK</button>
                             <button onClick={closeModal} className={props.content.cancelBtnClassName}>Cancel</button>
                         </div>
                     </div>
                 </div>
             </div>

        </div>
    )

}

Modal.propTypes = {
    action: PropTypes.func,
    content: PropTypes.object
}

export default Modal