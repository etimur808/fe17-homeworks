import React, {Component} from 'react';
import Button from "./Button";
import PropTypes, {checkPropTypes} from 'prop-types'

class Modal extends Component {
    constructor(props) {
        super(props);
        this.title = <Button/>.props.title;
        this.description = props.description;
        this.title = props.title
        this.btnClass = props.btnClass
        this.headerClass = props.headerClass
        this.contentClass = props.contentClass
        this.okBtnClass = props.okBtnClass
        this.cancelBtnClass = props.cancelBtnClass
        this.itemData = props.itemData
    }

    changeStateFalse = ()=>{
        this.props.action()
    }

    addToCart = (e) =>{
        if  (e.target.classList.contains('modal__addToCartOkBtn')){
            localStorage.setItem(`Add to cart ${this.itemData.title}`, JSON.stringify(this.itemData))
            this.changeStateFalse()
        }
    }

    closeModal = (e)=>{
        let isModal = e.target.className === "modal"
        let isCloseBtn = e.target.className === this.btnClass
        let isCancelBtn = e.target.className === this.cancelBtnClass

        if (isModal || isCloseBtn || isCancelBtn){
            this.changeStateFalse()
        }
    }

    render() {
        return (
            <div>
                <div onClick={this.closeModal} className={'modal'}>
                    <div className={this.contentClass}>
                        <div className={this.headerClass}>
                            <h1 className={'modal__title'}>{this.title}</h1>
                            <button className={this.btnClass}/>
                        </div>
                        <div className={'modal__body'}>
                            <p>{this.description}</p>

                            <div className={'modal__bodyButtons'}>

                            <button onClick={this.addToCart} className={this.okBtnClass} >Ok</button>
                            <button onClick={this.closeModal} className={this.cancelBtnClass} >Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

Modal.propTypes = {
    title: PropTypes.string,
    description: PropTypes.string,
    btnClass: PropTypes.string,
    action: PropTypes.func,
    headerClass: PropTypes.string,
    contentClass: PropTypes.string,
    okBtnClass: PropTypes.string,
    cancelBtnClass: PropTypes.string,
    itemData: PropTypes.object
}

Modal.defaultProps = {
    title: 'Modal is not found',
    description: '',
    btnClass: 'fas fa-times',
    headerClass: 'modal__header',
    contentClass: 'modal__content',
    okBtnClass: 'modal__bodyBtn',
    cancelBtnClass: 'modal__bodyBtn',
}

export default Modal