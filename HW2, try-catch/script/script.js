const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    },
];


// если честно, не понял как тут использовать try catch


function bookItemRender() {

    books.forEach(item => {
        let {name, author, price} = item;
        let booksData = {name, author, price};


        if (booksData.price === undefined || booksData.author === undefined || booksData.name === undefined) {
            for (let key in booksData) {
                if (booksData[key] === undefined) {
                    console.error("please add - ", key, "in this item - ", item);
                }
            }
        }
        else {
            const list = document.createElement("ul");
            const listWrapper = document.getElementById("root");
            const listItem = document.createElement('li');

            listWrapper.append(list);
            list.append(listItem);
            listItem.innerText = `${name}  ${author} ${price}`;

        }


    })
}

    bookItemRender();













